using Data.Context;
using Data.Entities;
using Domain.Exceptions;

namespace Data.Validations;

public class UserDbValidations
{
  public void ValidateExistingUsernameAndPassword(UserEntity? user, IDbContext context)
  {
    if (user == null || 
        context.Users.Any(dbUser => dbUser.Username == user.Username && user.Password == dbUser.Password) == null)
    {
      throw new ApiException("Invalid Credentials", "The username or password is incorrect", 403);
    }
  }

  public void ValidateUsernameExistence(UserEntity? user, IDbContext dbContext)
  {
    if (dbContext.Users.Any(dbuser => dbuser.Username == user.Username))
    {
      throw new ApiException("Invalid Credentials", "The username already exist", 409);
    }
  }

  public void ValidateUserExistenceById(int id, IDbContext context)
  {
    if (context.Users.Any(user => user.Id == id))
    {
      throw new ApiException("Not Found", $"The user whit id '{id}' does exist", 404);
    }
  }
}