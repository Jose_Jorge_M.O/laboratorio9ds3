using Data.Context;
using Data.Entities;
using Domain.Entities;
using Domain.Exceptions;

namespace Data.Validations;

public class GameDbValidator
{
  public void ValidateGamePrice(GameEntity game)
  {
    if (game.Price < 0)
    {
      throw new ApiParamException("Invalid Price", "Price must be greater than or equal to 0.", 400);
    }
  }

  public void ValidateUniqueGameName(GameEntity game, IDbContext dbContext)
  {
    if (dbContext.Games.Any(g => g.Name == game.Name))
    {
      throw new ApiDataConflictException("Duplicate Name", $"A game with the name '{game.Name}' already exists.", 409);
    }
  }

  public void ValidateGameList(IEnumerable<GameEntity> games)
  {
    if (games == null || !games.Any())
    {
      throw new ApiNotFoundException("Games Not Found", "The games list is empty.", 404);
    }
  }

  public void ValidateGameById(GameEntity? game, int id)
  {
    if (game == null)
    {
      throw new ApiNotFoundException("Game Not Found", $"A game with the ID '{id}' does not exist.", 404);
    }
  }
  
  public void ValidatePlatform(Platform? platform)
  {
    if (platform == null)
    {
      throw new ApiParamException("Empty Platform", "Platform cannot be empty.", 400);
    }
  }
}