using Data.Entities;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Data.Context;

public interface IDbContext
{
  public DbSet<UserEntity> Users { get;set; }
  public DbSet<GameEntity> Games { get;set; }
  public int SaveChanges();
}