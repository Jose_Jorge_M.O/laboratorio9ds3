using Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace Data.Context;

public class MemoryDbContext(DbContextOptions<MemoryDbContext> options) : DbContext(options), IDbContext
{

  public DbSet<GameEntity> Games { get; set; }
  public DbSet<UserEntity> Users { get; set; }

  protected override void OnModelCreating(ModelBuilder modelBuilder)
  {
    modelBuilder.Entity<GameEntity>()
      .HasIndex(e => e.Name)
      .IsUnique();
    modelBuilder.Entity<UserEntity>()
      .HasIndex(e => e.Username)
      .IsUnique();
  }
}