using AutoMapper;
using Data.Context;
using Data.Mapper;
using Data.Repository;
using Domain.Abtractions.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Data;

public static class DependencyInjectionService
{
  public static IServiceCollection AddPersistence(this IServiceCollection serviceCollection)
  {
    serviceCollection.AddDbContext<MemoryDbContext>(options =>
    {
      options.UseInMemoryDatabase("GamesDatabase");
    });

    serviceCollection.AddScoped<IDbContext, MemoryDbContext>();
    serviceCollection.AddScoped<IGameRepository, GameRepository>();
    serviceCollection.AddScoped<IUserRepository, UserRepository>();
    return serviceCollection;
  }

}