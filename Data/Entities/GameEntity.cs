using System.ComponentModel.DataAnnotations;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Data.Entities;

[Index("Name", IsUnique = true)]
public class GameEntity
{
  [Key]
  public int Id { get; set; }
  
  [Required]
  public string Name { get; set; }
  
  [Required]
  [EnumDataType(typeof(Platform))]
  public Platform Platform { get; set; }
  
  [Required]
  [Range(0, double.MaxValue)]
  public decimal Price { get; set; }
}
