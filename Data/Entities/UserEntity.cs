using System.ComponentModel.DataAnnotations;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Data.Entities;

[Index("Username", IsUnique = true)]
public class UserEntity
{
  [Key]
  public int Id { get; set; }
  [Required]
  public string Username { get; set; }
  [Required]
  public string Password { get; set; }
  [Required]
  [EnumDataType(typeof(Roles))]
  public Roles Role { get; set; }
}