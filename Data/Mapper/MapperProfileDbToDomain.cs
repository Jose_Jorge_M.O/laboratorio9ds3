using AutoMapper;
using Data.Entities;
using Domain.Entities;

namespace Data.Mapper;

public class MapperProfileDbToDomain : Profile
{
  public MapperProfileDbToDomain()
  {
    CreateMap<GameEntity, Game>().ReverseMap();
    CreateMap<Game, GameEntity>().ReverseMap();
    CreateMap<UserEntity, User>().ReverseMap();
    CreateMap<User, UserEntity>().ReverseMap();
  }
}