using System.Collections;
using AutoMapper;
using Data.Context;
using Data.Entities;
using Data.Validations;
using Domain.Abtractions.Repositories;
using Domain.Entities;


namespace Data.Repository;

public class GameRepository : IGameRepository
{

  private readonly IDbContext _dbContext;
  private readonly IMapper _mapper;
  private readonly GameDbValidator _gameValidator;

  public GameRepository(IDbContext dbContext, IMapper mapper)
  {
    _dbContext = dbContext;
    _mapper = mapper;
    _gameValidator = new GameDbValidator();
  }

  public Game Create(Game game)
  {
    var gameEntity = _mapper.Map<GameEntity>(game);
    _gameValidator.ValidateGamePrice(gameEntity);
    _gameValidator.ValidatePlatform(gameEntity.Platform);
    _gameValidator.ValidateUniqueGameName(gameEntity, _dbContext);
    _dbContext.Games.Add(gameEntity);
    _dbContext.SaveChanges();
    return _mapper.Map<Game>(gameEntity);
    
  }

  public Game GetById(int id)
  {
    var gameEntity =  _dbContext.Games.FirstOrDefault(g => g.Id == id);
    _gameValidator.ValidateGameById(gameEntity, id);
    return _mapper.Map<Game>(gameEntity);
  }


  public IEnumerable<Game> GetAll()
  {
    IEnumerable<GameEntity> games = _dbContext.Games.ToList();
    _gameValidator.ValidateGameList(games);
    return _mapper.Map<List<Game>>(games);
  }


  public void Update(Game game)
  {
    _gameValidator.ValidateUniqueGameName(_mapper.Map<GameEntity>(game), _dbContext);

    var existingGame = _dbContext.Games.Find(game.Id);
    
    _gameValidator.ValidateGameById(existingGame, game.Id);
    _gameValidator.ValidateGamePrice(existingGame);
    _gameValidator.ValidatePlatform(existingGame.Platform);
    
    existingGame.Name = game.Name;
    existingGame.Price = game.Price;
    existingGame.Platform = game.Platform;

    _dbContext.SaveChanges();

  }

  public void Delete(int id)
  {
    var gameToDelete = _dbContext.Games.Find(id);
    
    _gameValidator.ValidateGameById(gameToDelete, id);
    
    _dbContext.Games.Remove(gameToDelete);
    _dbContext.SaveChanges();
  }

}