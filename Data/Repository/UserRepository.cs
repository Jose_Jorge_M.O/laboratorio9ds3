using AutoMapper;
using Data.Context;
using Data.Entities;
using Data.Validations;
using Domain.Abtractions.Repositories;
using Domain.Entities;

namespace Data.Repository;

public class UserRepository : IUserRepository
{
  private readonly IDbContext _dbContext;
  private readonly IMapper _mapper;
  private readonly UserDbValidations _validations;

  public UserRepository(IDbContext dbContext, IMapper mapper)
  {
    _dbContext = dbContext;
    _mapper = mapper;
    _validations = new UserDbValidations();
  }
  
  public User GetUserByUsernameAndPassword(string username, string password)
  {
    UserEntity userEntity = _dbContext.Users.FirstOrDefault(user => user.Password == password && user.Username == username);
    _validations.ValidateExistingUsernameAndPassword(userEntity, _dbContext);
    return _mapper.Map<User>(userEntity);
  }

  public IEnumerable<User> GetUsers()
  {
    return _mapper.Map<List<User>>(_dbContext.Users.ToList());
  }

  public User Create(User user)
  {
    var userEntity = _mapper.Map<UserEntity>(user);
    _validations.ValidateUsernameExistence(userEntity, _dbContext);
    _dbContext.Users.Add(userEntity);
    _dbContext.SaveChanges();
    return _mapper.Map<User>(userEntity);
  }

  public void Delete(int userId)
  {
    _validations.ValidateUserExistenceById(userId, _dbContext);
    _dbContext.Users.Remove(_dbContext.Users.Find(userId));
    _dbContext.SaveChanges();
  }
}