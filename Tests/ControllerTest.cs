using Application.Services;
using AutoMapper;
using Data.Context;
using Data.Entities;
using Data.Mapper;
using Data.Repository;
using Domain.Entities;
using JuegosAPI.Controllers;
using JuegosAPI.DTOs;
using JuegosAPI.Mapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Tests;

public class ControllerTest
{
  private MemoryDbContext _dbContext;
  private GameController _controller;
  private void Populate(MemoryDbContext _dbContext)
  {
    var game = new GameEntity() { Name = "game", Platform = Platform.Xbox, Price = 30};
    var game2 = new GameEntity() { Name = "game2", Platform = Platform.Xbox, Price = 300};
    
    _dbContext.Add(game);
    _dbContext.Add(game2);
    
    _dbContext.SaveChanges();
  }

  private IMapper GetMapper()
  {
    return new Mapper(new MapperConfiguration(
      config =>
      {
        config.AddProfile(new MapperProfileDbToDomain());
        config.AddProfile(new MapperProfileDtoToDomain());
      }));
  }
  
  public void SetUp()
  {
    var options = new DbContextOptionsBuilder<MemoryDbContext>()
      .UseInMemoryDatabase(databaseName: "TestDatabase")
      .Options;
    _dbContext =  new MemoryDbContext(options);
    var service = new GameService(new GameRepository(_dbContext, GetMapper()));
    _dbContext.Database.EnsureDeleted();
    Populate(_dbContext);
    _controller = new GameController(service, GetMapper());
  }
  
  private void ClearDatabase()
  {
    _dbContext.Database.EnsureDeleted();
    _dbContext.SaveChanges();
  }
  
  [Fact]
  public void Create_ValidGame_ReturnsCreatedGame()
  {
    SetUp();
    var inputGame = new GameInfo { Name = "Test Game", Price = 10, Platform = Platform.Xbox.ToString()}; 
    var response = (_controller.CreateNewGame(inputGame) as CreatedResult);
    var createdGame = response.Value as ResponseData<EntityResponse<GameAttributes>> ;
    Assert.NotNull(createdGame);
    Assert.Equal(3, createdGame.Data.Id);
    Assert.Equal("game", createdGame.Data.Type);
    Assert.Equal(inputGame.Platform, createdGame.Data.Attributes.Platform.ToString());
    Assert.Equal(inputGame.Name, createdGame.Data.Attributes.Name);
    Assert.Equal(StatusCodes.Status201Created, response.StatusCode);
  }

  [Fact]
  public void GetAll_ReturnsAllGames()
  {
    SetUp();
    var response =
      (_controller.GetAllGames() as OkObjectResult);
    var responseValue = response.Value as ResponseData<List<EntityResponse<GameAttributes>>>;
    Assert.NotNull(response);
    Assert.Equal(2, responseValue.Data.Count());
    Assert.Equal("game", responseValue.Data[0].Type);
    Assert.Equal(Platform.Xbox, responseValue.Data[0].Attributes.Platform);
    Assert.Equal(1, responseValue.Data[0].Id);
    Assert.Equal("game", responseValue.Data[0].Attributes.Name);
    Assert.Equal(StatusCodes.Status200OK, response.StatusCode);
  }

  [Fact]
  public void GetGameById_ReturnGame()
  {
    SetUp();
    var inputGame = new GameInfo { Name = "Test Game", Price = 10, Platform = Platform.Xbox.ToString()}; 
    var response =
      (_controller.UpdateGame(inputGame,1) as OkObjectResult); 
    var responseValue = response.Value as ResponseData<EntityResponse<GameAttributes>>;
    Assert.NotNull(response);
    Assert.Equal(1, responseValue.Data.Id);
    Assert.Equal("game", responseValue.Data.Type);
    Assert.Equal(Platform.Xbox, responseValue.Data.Attributes.Platform);
    Assert.Equal(inputGame.Name, responseValue.Data.Attributes.Name);
    Assert.Equal(StatusCodes.Status200OK, response.StatusCode);
  }
  
  [Fact]
  public void UpdateGame_ReturnUpdatedGame()
  {
    SetUp();
    var response = (_controller.GetGameById(1) as OkObjectResult);
    var responseValue = response.Value as ResponseData<EntityResponse<GameAttributes>>;
    Assert.NotNull(response);
    Assert.Equal(1, responseValue.Data.Id);
    Assert.Equal("game", responseValue.Data.Type);
    Assert.Equal(Platform.Xbox, responseValue.Data.Attributes.Platform);
    Assert.Equal("game", responseValue.Data.Attributes.Name);
    Assert.Equal(StatusCodes.Status200OK, response.StatusCode);
  }
  
  [Fact]
  public void Delete_ReturnVoid()
  {
    SetUp();

    var response = (_controller.DeleteGame(1) as OkResult);

    Assert.NotNull(response);
    Assert.Equal(StatusCodes.Status200OK, response.StatusCode);  }
}