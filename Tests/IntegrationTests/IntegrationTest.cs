using System.Text;
using JuegosAPI.DTOs;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http.Headers;

namespace Tests.IntegrationTests;

public class IntegrationTest : IClassFixture<WebApplicationFactory<Program>>
{
  private readonly HttpClient _client;

  public IntegrationTest(WebApplicationFactory<Program> factory)
  {
    _client = factory.CreateClient();
  }

  protected StringContent CreateRequestContent(object objet)
  {
    var json = JsonConvert.SerializeObject(objet);
    var content = new StringContent(json, Encoding.UTF8, "application/json");
    return content;
  }
  
  [Fact]
  public async Task TestIntegration()
  {
    // Registro de usuario
    var user = new UserInfoToCreate()
    {
      Username = "jose",
      Password = "12345678",
      Role = "Admin"
    };
    var userContent = CreateRequestContent(user);
    var registerResponse = await _client.PostAsync("api/v1/registro", userContent);
    Assert.Equal(HttpStatusCode.Created,registerResponse.StatusCode);

    // Iniciar sesión y obtener token
    HttpResponseMessage loginResponse = await _client.PostAsync("api/v1/login",
      CreateRequestContent(new UserInfo() { Username = user.Username, Password = user.Password }));
    Assert.Equal(HttpStatusCode.OK,loginResponse.StatusCode);
    // check response structure 
    var loginResponseBody = await loginResponse.Content.ReadAsAsync<LoginResponse>();
    Assert.NotNull(loginResponseBody);
    var token = loginResponseBody.Token;
    Assert.NotNull(token);
    _client.DefaultRequestHeaders.Authorization =
      new AuthenticationHeaderValue("Bearer", token);

    // Crear juegos
    var game1 = new GameInfo()
    {
      Name = "Super Mario Bros",
      Platform = "Xbox",
      Price = 1985
    };
    var game2 = new GameInfo()
    {
      Name = "The Legend of Zelda: Ocarina of Time",
      Platform = "Xbox",
      Price = 1998
    };
    var gameContent1 = CreateRequestContent(game1);
    var gameContent2 = CreateRequestContent(game2);
    var createGameResponse1 = await _client.PostAsync("api/v1/juegos", gameContent1);
    var createGameResponse2 = await _client.PostAsync("api/v1/juegos", gameContent2);
    // check response structure 
    var response = await createGameResponse1.Content.ReadAsAsync<ResponseData<EntityResponse<GameAttributes>>>();
    Assert.NotNull(response);
    Assert.Equal(game1.Name, response.Data.Attributes.Name);
    Assert.Equal(HttpStatusCode.Created, createGameResponse1.StatusCode);
    Assert.Equal(HttpStatusCode.Created, createGameResponse2.StatusCode);


    // Obtener juegos
    var getGamesResponse = await _client.GetAsync("api/v1/juegos");
    getGamesResponse.EnsureSuccessStatusCode();
    // check response structure 
    var getGamesResponseBody = await getGamesResponse.Content.ReadAsAsync<ResponseData<List<EntityResponse<GameAttributes>>>>();
    Assert.NotNull(getGamesResponseBody);
    Assert.Equal(2, getGamesResponseBody.Data.Count);
    var games = getGamesResponseBody.Data;

    // Verificar nombres de los juegos
    Assert.Equal(game1.Name, games[0].Attributes.Name);
    Assert.Equal(game2.Name, games[1].Attributes.Name);
  }
}

public class LoginResponse
{
  public string Token { get; set; }
}