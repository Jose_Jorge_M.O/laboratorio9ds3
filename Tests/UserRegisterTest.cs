using Application.Services;
using AutoMapper;
using Data.Context;
using Data.Entities;
using Data.Mapper;
using Data.Repository;
using Domain.Abstractions.Services;
using Domain.Abtractions.Repositories;
using Domain.Entities;
using JuegosAPI.Controllers;
using JuegosAPI.DTOs;
using JuegosAPI.Mapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Moq;

namespace Tests;

public class UserRegisterTest
{
  private MemoryDbContext _dbContext;
  private AuthController _controller;
  private void Populate(MemoryDbContext _dbContext)
  {
    var game = new GameEntity() { Name = "game", Platform = Platform.Xbox, Price = 30};
    var game2 = new GameEntity() { Name = "game2", Platform = Platform.Xbox, Price = 300};
    var user = new UserEntity() { Username = "jose", Password = "3dfsfsd89", Role = Roles.User, Id = 21};
    
    _dbContext.Add(game);
    _dbContext.Add(game2);
    _dbContext.Add(user);
    
    _dbContext.SaveChanges();
  }

  private IMapper GetMapper()
  {
    return new Mapper(new MapperConfiguration(
      config =>
      {
        config.AddProfile(new MapperProfileDbToDomain());
        config.AddProfile(new MapperProfileDtoToDomain());
      }));
  }
  
  private void SetUp()
  {
    var configuration = new Mock<IConfiguration>();
    var settingsSection = new Mock<IConfigurationSection>();
    configuration.Setup(x => x.GetSection("settings")).Returns(settingsSection.Object);
    var secretKeySection = new Mock<IConfigurationSection>();
    settingsSection.Setup(x => x.GetSection("secretkey")).Returns(secretKeySection.Object);
    secretKeySection.Setup(x => x.ToString()).Returns("SomeSecretKey");

    var options = new DbContextOptionsBuilder<MemoryDbContext>()
      .UseInMemoryDatabase(databaseName: "TestDatabasef")
      .Options;
    _dbContext =  new MemoryDbContext(options);
    var service = new UserService(new UserRepository(_dbContext, GetMapper()));
    _dbContext.Database.EnsureDeleted();
    Populate(_dbContext);
    _controller = new AuthController(configuration.Object, service, GetMapper());
  }

  [Fact]
  public void Register_InvalidUser_UserAlreadyExist()
  {
    SetUp();
    var httpContext = new DefaultHttpContext();
    httpContext.Request.Path = "/api/v1/registro"; 
    _controller.ControllerContext = new ControllerContext()
    {
      HttpContext = httpContext
    };
    var inputUser = new UserInfoToCreate() {Username = "jose", Password = "123jdshsuyqw", Role = "Admin"};
    var response = _controller.CreateUser(inputUser) as ObjectResult;
    Assert.Equal(409, response.StatusCode);
    Assert.Equal("Invalid Credentials", (response.Value as ProblemDetails).Title);
    Assert.Equal("The username already exist", (response.Value as ProblemDetails).Detail);
  }

}