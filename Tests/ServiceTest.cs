using Application.Services;
using AutoMapper;
using Data.Context;
using Data.Entities;
using Data.Mapper;
using Data.Repository;
using Domain.Abstractions.Services;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Tests;

public class ServiceTest
{
  private readonly MemoryDbContext _dbContext;
  private readonly IGameService _service;
  
  private void Populate()
  {
    var game = new GameEntity() { Name = "game", Platform = Platform.Xbox, Price = 30};
    var game2 = new GameEntity() { Name = "game2", Platform = Platform.Xbox, Price = 300};
    
    _dbContext.Add(game);
    _dbContext.Add(game2);
    
    _dbContext.SaveChanges();
  }

  private IMapper GetMapper()
  {
    return new Mapper(new MapperConfiguration(
      config =>
      {
        config.AddProfile(new MapperProfileDbToDomain());
      }));
  }

  public ServiceTest()
  {
    var options =new DbContextOptionsBuilder<MemoryDbContext>()
      .UseInMemoryDatabase(databaseName: "TestDatabase3")
      .Options;
    _dbContext =  new MemoryDbContext(options);
    _service = new GameService(new GameRepository(_dbContext, GetMapper()));
  }
  
  [Fact]
  public void Create_ValidGame_ReturnsCreatedGame()
  {
    _dbContext.Database.EnsureDeleted();
    var inputGame = new Game {Id = 1, Name = "Test Game", Price = 10.0m, Platform = Platform.Xbox};
    
    var createdGame = _service.CreateGame(inputGame);

    Assert.NotNull(createdGame);
    Assert.Equal(inputGame.Id, createdGame.Id);
    Assert.Equal(inputGame.Name, createdGame.Name);
    Assert.Equal(inputGame.Price, createdGame.Price);
  }
  
  [Fact]
  public void GetAll_ReturnsAllGames()
  {
    _dbContext.Database.EnsureDeleted();
    Populate();
    var allGames = _service.GetAllGames().ToList();

    Assert.NotNull(allGames);
    Assert.Equal(2, allGames.Count());
  }
  
  [Fact]
  public void GetGameById_ReturnGame()
  {

    _dbContext.Database.EnsureDeleted();
    Populate();
    Game game = _service.GetGameById(1);

    Assert.NotNull(game);
    Assert.Equal(1, game.Id);
    Assert.Equal("game", game.Name);
    Assert.Equal(30, game.Price);
  }
  
  [Fact]
  public void UpdateGame_ReturnUpdatedGame()
  {
    _dbContext.Database.EnsureDeleted();
    Populate();
    Game newGame = new Game { Id = 1, Name = "newName", Platform = Platform.PlayStation, Price = 302 };
    _service.UpdateGame(newGame);
    
  }
  
  [Fact]
  public void Delete_ReturnVoid()
  {
    _dbContext.Database.EnsureDeleted();
    Populate();
    _service.DeleteGame(1);
  }
}