using AutoMapper;
using Data.Context;
using Data.Entities;
using Data.Mapper;
using Data.Repository;
using Data.Validations;
using Domain.Abtractions.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Moq;

namespace Tests;

public class RepositoryTest
{

  private readonly MemoryDbContext _dbContext;
  private readonly IGameRepository _repository;
  
  private void Populate()
  {
    var game = new GameEntity() { Name = "game", Platform = Platform.Xbox, Price = 30};
    var game2 = new GameEntity() { Name = "game2", Platform = Platform.Xbox, Price = 300};
    
    _dbContext.Add(game);
    _dbContext.Add(game2);
    
    _dbContext.SaveChanges();
  }
  
  private void ClearDatabase()
  {
    _dbContext.Database.EnsureDeleted();
    _dbContext.SaveChanges();
  }

  private IMapper GetMapper()
  {
    return new Mapper(new MapperConfiguration(
      config =>
      {
        config.AddProfile(new MapperProfileDbToDomain());
      }));
  }

  public RepositoryTest()
  {
    var options =new DbContextOptionsBuilder<MemoryDbContext>()
      .UseInMemoryDatabase(databaseName: "TestDatabase2")
      .Options;
    _dbContext =  new MemoryDbContext(options);
    _repository = new GameRepository(_dbContext, GetMapper());

  }

  [Fact]
  public void Create_ValidGame_ReturnsCreatedGame()
  {
    ClearDatabase();
    var inputGame = new Game { Id = 1, Name = "Test Game", Price = 10.0m, Platform = Platform.Xbox};
    
    var createdGame = _repository.Create(inputGame);

    Assert.NotNull(createdGame);
    Assert.Equal(1, createdGame.Id);
    Assert.Equal(inputGame.Name, createdGame.Name);
    Assert.Equal(inputGame.Price, createdGame.Price);
  }
  
  [Fact]
  public void GetAll_ReturnsAllGames()
  {
    ClearDatabase();
    Populate();
    var allGames = _repository.GetAll().ToList();

    Assert.NotNull(allGames);
    Assert.Equal(2, allGames.Count());
  }
  
  [Fact]
  public void GetGameById_ReturnGame()
  {

    ClearDatabase();
    Populate();
    Game game = _repository.GetById(1);

    Assert.NotNull(game);
    Assert.Equal(1, game.Id);
    Assert.Equal("game", game.Name);
    Assert.Equal(30, game.Price);
  }
  
  [Fact]
  public void UpdateGame_ReturnUpdatedGame()
  {
    ClearDatabase();
    Populate();
    Game newGame = new Game { Id = 1, Name = "newName", Platform = Platform.PlayStation, Price = 302 };
    _repository.Update(newGame);
    
  }
  
  [Fact]
  public void Delete_ReturnVoid()
  {
    ClearDatabase();
    Populate();
    _repository.Delete(1);
  }
}