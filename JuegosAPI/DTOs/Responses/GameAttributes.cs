using System.Text.Json.Serialization;
using Domain.Entities;

namespace JuegosAPI.DTOs;

public class GameAttributes
{
  public string Name { get; set; }
  [JsonConverter(typeof(JsonStringEnumConverter))]
  public Platform Platform { get; set; }
  public decimal Price { get; set; }
}