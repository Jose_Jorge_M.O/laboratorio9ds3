namespace JuegosAPI.DTOs;

public class EntityResponse<T>
{
  public string Type { get; set; }
  public int Id { get; set; }
  public T Attributes { get; set; }

  public EntityResponse()
  {
  }

  public EntityResponse(int id, T attributes, string type = null)
  {
    Id = id;
    Attributes = attributes;
    Type = type;
  }
}