using System.Text.Json.Serialization;
using Domain.Entities;

namespace JuegosAPI.DTOs;

public class UserAttribues
{
  public string Usename { get; set; }
  public string Password { get; set; }
  [JsonConverter(typeof(JsonStringEnumConverter))]
  public Roles Role { get; set; }
}