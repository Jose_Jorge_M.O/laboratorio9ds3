namespace JuegosAPI.DTOs;

public class UserInfoToCreate : UserInfo
{
  public string Role { get; set; }
}