namespace JuegosAPI.DTOs;

public class GameInfo
{
  public string Name { get; set; }
  public string Platform { get; set; }
  public decimal Price { get; set; }
}