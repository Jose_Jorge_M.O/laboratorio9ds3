namespace JuegosAPI.DTOs;

public class UserInfo
{
  public string Username { get; set; }
  public string Password { get; set; }
}