using Domain.Entities;
using Microsoft.IdentityModel.Tokens;

namespace JuegosAPI.Validations;

public class FieldsValdiator
{
  public static bool IsValidPlatform(string value, out Platform platform)
  {
    platform = Platform.Xbox;
    
    foreach (Platform plat in Enum.GetValues(typeof(Platform)))
    {
      if (string.Equals(value, plat.ToString(), StringComparison.OrdinalIgnoreCase))
      {
        platform = plat;
        return true;
      }
    }
    return false;
  }

  public static bool IsEmpty(string field)
  {
    return field.IsNullOrEmpty();
  }

  public static bool IsValidRole(string value, out Roles result)
  {
    result = Roles.User;
    
    foreach (Roles rol in Enum.GetValues(typeof(Roles)))
    {
      if (string.Equals(value, rol.ToString(), StringComparison.OrdinalIgnoreCase))
      {
        result = rol;
        return true;
      }
    }
    return false;
  }
}