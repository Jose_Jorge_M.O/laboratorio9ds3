using AutoMapper;
using Domain.Abstractions.Services;
using Domain.Entities;
using Domain.Exceptions;
using JuegosAPI.DTOs;
using JuegosAPI.Validations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace JuegosAPI.Controllers;

[ApiController]
[Route("api/v1/juegos")]
public class GameController : ControllerBase
{
  private readonly IGameService _service;
  private readonly IMapper _mapper;

  public GameController(IGameService gameService, IMapper mapper)
  {
    _mapper = mapper;
    _service = gameService;
  }

  [HttpGet]
  public IActionResult GetAllGames()
  {
    try
    {
      var games = _service.GetAllGames().ToList();
      var response = _mapper.Map<List<EntityResponse<GameAttributes>>>(games);
      return Ok(new ResponseData<List<EntityResponse<GameAttributes>>>(response));
    }
    catch (ApiException exception)
    {
      var problemDetails = new ProblemDetails
      {
        Title = exception.Title,
        Detail = exception.Message,
        Status = exception.Status,
        Type = "https://juegosAPI.com/errors/",
        Instance = HttpContext.Request.Path
      };
      return StatusCode(exception.Status,problemDetails);
    }
  }
  
  [HttpGet]
  [Route("/juegos/{id:int}")]
  [Authorize]
  public IActionResult GetGameById([FromRoute] int id)
  {
    try
    {
      var game = _service.GetGameById(id);
      var response = _mapper.Map<EntityResponse<GameAttributes>>(game);
      return Ok(new ResponseData<EntityResponse<GameAttributes>>(response));
    }
    catch (ApiException exception)
    {
      var problemDetails = new ProblemDetails
      {
        Title = exception.Title,
        Detail = exception.Message,
        Status = exception.Status,
        Type = "https://juegosAPI.com/errors/",
        Instance = HttpContext.Request.Path
      };
      return StatusCode(exception.Status,problemDetails);
    }
  }
  
  [HttpPost]
  [Authorize(Policy = "RequireAdminRole")]
  public IActionResult CreateNewGame([FromBody] GameInfo info)
  {
    try
    {
      Platform platform;
      if (FieldsValdiator.IsValidPlatform(info.Platform, out platform))
      {
        info.Platform = platform.ToString();
      }
      else
      {
        throw new ApiParamException("Invalid Platform Value",
          "The Platform Value Is Invalid the expected values are: Xbox or PlayStation", 400);
      }

      Game game = _service.CreateGame(_mapper.Map<Game>(info));
      var response = _mapper.Map<EntityResponse<GameAttributes>>(game);
      return Created(uri: $"/juegos/{game.Id}", new ResponseData<EntityResponse<GameAttributes>>(response));
    }
    catch (ApiException exception)
    {
      var problemDetails = new ProblemDetails
      {
        Title = exception.Title,
        Detail = exception.Message,
        Status = exception.Status,
        Type = "https://juegosAPI.com/errors/",
        Instance = HttpContext.Request.Path
      };
      return StatusCode(exception.Status, problemDetails);
    }
    catch (Exception e)
    {
      Console.WriteLine(e.Message);
      return Ok();
    }
  }

  [HttpPut]
  [Route("/juegos/{id:int}")]
  [Authorize(Policy = "RequireAdminRole")]
  public IActionResult UpdateGame([FromBody] GameInfo info, [FromRoute] int id)
  {
    try
    {
      Platform platform;
      if (FieldsValdiator.IsValidPlatform(info.Platform, out platform))
      {
        info.Platform = platform.ToString();
      }
      else
      {
        throw new ApiParamException("Invalid Platform Value",
          "The Platform Value Is Invalid the expected values are: Xbox or PlayStation", 400);
      }
      Game game = _mapper.Map<Game>(info);
      game.Id = id;
      _service.UpdateGame(game);
      var response = _mapper.Map<EntityResponse<GameAttributes>>(game);
      return Ok(new ResponseData<EntityResponse<GameAttributes>>(response));
    }
    catch (ApiException exception)
    {
      var problemDetails = new ProblemDetails
      {
        Title = exception.Title,
        Detail = exception.Message,
        Status = exception.Status,
        Type = "https://juegosAPI.com/errors/",
        Instance = HttpContext.Request.Path
      };
      return StatusCode(exception.Status,problemDetails);
    }
  }

  [HttpDelete]
  [Route("/juegos/{id:int}")]
  [Authorize(Policy = "RequireAdminRole")]
  public IActionResult DeleteGame([FromRoute] int id)
  {
    try
    {
      _service.DeleteGame(id);
      return Ok();
    }
    catch (ApiException exception)
    {
      var problemDetails = new ProblemDetails
      {
        Title = exception.Title,
        Detail = exception.Message,
        Status = exception.Status,
        Type = "https://juegosAPI.com/errors/",
        Instance = HttpContext.Request.Path
      };
      return StatusCode(exception.Status,problemDetails);
    }
  }
}