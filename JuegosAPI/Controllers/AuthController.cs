using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using AutoMapper;
using Domain.Abstractions.Services;
using Domain.Entities;
using Domain.Exceptions;
using JuegosAPI.DTOs;
using JuegosAPI.Validations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace JuegosAPI.Controllers;

[ApiController]
[Route("api/v1/")]
public class AuthController : ControllerBase
{
  private readonly string _secretKey;
  private readonly IUserService _service;
  private readonly IMapper _mapper;

  public AuthController(IConfiguration configuration, IUserService service, IMapper mapper)
  {
    _mapper = mapper;
    _secretKey = configuration.GetSection("settings").GetSection("secretkey").ToString();
    _service = service;
  }

  [HttpPost]
  [Route("login")]
  public IActionResult Login([FromBody] UserInfo info)
  {
    try
    {
      User user = _service.GetUserByUsernameAndPassword(info.Username, info.Password);
      var keyBytes = Encoding.ASCII.GetBytes(_secretKey);
      var claims = new ClaimsIdentity();
      claims.AddClaim(new Claim(ClaimTypes.NameIdentifier, info.Username));
      if (user.Role == Roles.Admin)
      {
        claims.AddClaim(new Claim(ClaimTypes.Role, "Admin"));
      }
      else
      {
        claims.AddClaim(new Claim(ClaimTypes.Role, "User"));
      }
      var tokenDescriptor = new SecurityTokenDescriptor
      {
        Subject = claims,
        Expires = DateTime.UtcNow.AddMinutes(10),
        SigningCredentials =
          new SigningCredentials(new SymmetricSecurityKey(keyBytes), SecurityAlgorithms.HmacSha256Signature)
      };
      var tokeHandler = new JwtSecurityTokenHandler();
      var tokenConfig = tokeHandler.CreateToken(tokenDescriptor);
      string token = tokeHandler.WriteToken(tokenConfig);
      return StatusCode(StatusCodes.Status200OK, new { token = token });
    }
    catch (ApiException exception)
    {
      var problemDetails = new ProblemDetails
      {
        Title = exception.Title,
        Detail = exception.Message,
        Status = exception.Status,
        Type = "https://juegosAPI.com/errors/",
        Instance = HttpContext.Request.Path
      };
      return StatusCode(exception.Status,problemDetails);
    }
  }
  
  [HttpPost]
  [Route("registro")]
  public IActionResult CreateUser([FromBody] UserInfoToCreate info)
  {
    try
    {
      Roles roles;
      if (FieldsValdiator.IsValidRole(info.Role, out roles))
      {
        info.Role = roles.ToString();
      }
      else
      {
        throw new ApiParamException("Invalid Rol Value",
          "The Rol Value Is Invalid the expected values are: Admin or User");
      }

      var user = _service.CreateUser(_mapper.Map<User>(info));
      return Created($"/usuarios/{user.Id}",
        new ResponseData<EntityResponse<UserAttribues>>(_mapper.Map<EntityResponse<UserAttribues>>(user)));
    }
    catch (ApiException exception)
    {
      var problemDetails = new ProblemDetails
      {
        Title = exception.Title,
        Detail = exception.Message,
        Status = exception.Status,
        Type = "https://juegosAPI.com/errors/",
        Instance = HttpContext.Request.Path
      };
      return StatusCode(exception.Status,problemDetails);
    }
  }
}