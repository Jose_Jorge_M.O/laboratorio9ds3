using System.Reflection;
using AutoMapper;
using Data.Mapper;
using JuegosAPI.Mapper;
using Microsoft.OpenApi.Models;

namespace JuegosAPI;

public static class DependencyInjectionService
{
  public static IServiceCollection AddWebApi(this IServiceCollection service)
  {
    service.AddSwaggerGen(options => 
    {
      options.SwaggerDoc("v1", new OpenApiInfo
      {
        Version = "v1",
        Title = " Juegos Crud",
        Description = " Juegos API"
      });

      var fileName = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
      options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, fileName));
    });
    var mapper = new MapperConfiguration(config =>
    {
      config.AddProfile(new MapperProfileDtoToDomain());
      config.AddProfile(new MapperProfileDbToDomain());
    });

    service.AddSingleton(mapper.CreateMapper());
    
    return service;
  }
}