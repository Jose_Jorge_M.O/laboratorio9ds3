using AutoMapper;
using Data.Entities;
using Domain.Entities;
using JuegosAPI.DTOs;

namespace JuegosAPI.Mapper;

public class MapperProfileDtoToDomain : Profile
{
  public MapperProfileDtoToDomain()
  {
    CreateMap<Game, GameInfo>().ReverseMap();
    CreateMap<User, UserInfoToCreate>().ReverseMap();
    CreateMap<Game, EntityResponse<GameAttributes>>()
      .ForMember(dest => dest.Type, opt => opt.MapFrom(src => "game"))
      .ForMember(dest => dest.Attributes, opt => opt.MapFrom(src => new GameAttributes
      {
        Name = src.Name,
        Platform = src.Platform,
        Price = src.Price
      }));
    
    CreateMap<User, EntityResponse<UserAttribues>>()
      .ForMember(dest => dest.Type, opt => opt.MapFrom(src => "user"))
      .ForMember(dest => dest.Attributes, opt => opt.MapFrom(src => new UserAttribues()
      {
        Usename = src.Username,
        Role = src.Role,
        Password = src.Password
      }));
  }
}