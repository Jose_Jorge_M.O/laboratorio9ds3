using Domain.Abstractions.Services;
using Domain.Abtractions.Repositories;
using Domain.Entities;

namespace Application.Services;

public class GameService : IGameService
{

  private readonly IGameRepository _gameRepository;
  private readonly GameValidator _gameValidator;

  public GameService(IGameRepository gameRepository)
  {
    _gameRepository = gameRepository;
    _gameValidator = new GameValidator();
  }

  public Game CreateGame(Game game)
  {
    _gameValidator.ValidateGame(game);
    return _gameRepository.Create(game);
  }

  public IEnumerable<Game> GetAllGames()
  {
    return _gameRepository.GetAll().ToList();
  }

  public Game GetGameById(int gameId)
  {
    _gameValidator.ValidateGameId(gameId);
    return _gameRepository.GetById(gameId);
  }

  public void UpdateGame(Game game)
  {
    _gameValidator.ValidateGame(game);
    _gameValidator.ValidateGameId(game.Id);
    _gameRepository.Update(game);
  }

  public void DeleteGame(int gameId)
  {
    _gameValidator.ValidateGameId(gameId);
    _gameRepository.Delete(gameId);
  }
}