using Application.Validations;
using Domain.Abstractions.Services;
using Domain.Abtractions.Repositories;
using Domain.Entities;

namespace Application.Services;

public class UserService : IUserService
{
  private readonly IUserRepository _repository;
  private readonly UserValidator _validator;

  public UserService(IUserRepository repository)
  {
    _repository = repository;
    _validator = new UserValidator();
  } 
  public User GetUserByUsernameAndPassword(string username, string password)
  {
    _validator.ValidatePassword(password);
    return _repository.GetUserByUsernameAndPassword(username,password);
  }

  public User CreateUser(User user)
  {
    _validator.ValidatePassword(user.Password);
    return _repository.Create(user);
  }

  public IEnumerable<User> GetAllUsers()
  {
    return _repository.GetUsers();
  }

  public void DeleteUser(int userId)
  { 
    _repository.Delete(userId);
  }
}