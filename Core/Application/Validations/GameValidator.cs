using Domain.Exceptions;
using Domain.Entities;

public class GameValidator
{
  public void ValidateGame(Game game)
  {
    if (game == null)
    {
      throw new ApiParamException("Null Parameter", "Game entity cannot be null.", 400);
    }

    ValidatePrice(game.Price);
    ValidateName(game.Name);
    ValidatePlatform(game.Platform);
  }

  public void ValidateGameId(int id)
  {
    ValidateId(id);
  }

  private void ValidatePrice(decimal? price)
  {
    if (price is < 0 or null)
    {
      throw new ApiParamException("Invalid Price", "Price must be greater than or equal to 0.", 400);
    }
  }

  private void ValidateId(int? id)
  {
    if (id is <= 0 or null)
    {
      throw new ApiParamException("Invalid ID", "ID must be greater than 0.", 400);
    }
  }

  private void ValidateName(string? name)
  {
    if (string.IsNullOrWhiteSpace(name))
    {
      throw new ApiParamException("Empty Name", "Name cannot be empty.", 400);
    }
  }

  private void ValidatePlatform(Platform? platform)
  {
    if (platform == null)
    {
      throw new ApiParamException("Empty Platform", "Platform cannot be empty.", 400);
    }
  }
}