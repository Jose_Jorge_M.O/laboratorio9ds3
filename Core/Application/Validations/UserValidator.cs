using Domain.Exceptions;

namespace Application.Validations;

public class UserValidator
{
  public void ValidatePassword(string password)
  {
    if (password.Length < 8)
    {
      throw new ApiParamException("Invalid password", "Password must be at least 8 characters long.");
    }

    bool containsNumber = false;
    foreach (char c in password)
    {
      if (char.IsDigit(c))
      {
        containsNumber = true;
        break;
      }
    }

    if (!containsNumber)
    {
      throw new ApiParamException("Invalid password",
        "Password must contain at least one digit.");
    }
    
  }
}