using Application.Services;
using Domain.Abstractions.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Application;

public static class DependencyInjectionService
{
  public static IServiceCollection AddApplication(this IServiceCollection serviceCollection)
  {
    serviceCollection.AddTransient<IGameService, GameService>();
    serviceCollection.AddTransient<IUserService, UserService>();
    return serviceCollection;
  }
}