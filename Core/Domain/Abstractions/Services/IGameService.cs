using Domain.Entities;

namespace Domain.Abstractions.Services
{
  public interface IGameService
  {
    Game CreateGame(Game game);

    IEnumerable<Game> GetAllGames();

    Game GetGameById(int gameId);

    void UpdateGame(Game game);

    void DeleteGame(int gameId);
  }
}