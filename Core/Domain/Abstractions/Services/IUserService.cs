using Domain.Entities;

namespace Domain.Abstractions.Services;

public interface IUserService
{
  User GetUserByUsernameAndPassword(string username, string password);
  
  User CreateUser(User user);
  
  IEnumerable<User> GetAllUsers();
  
  void DeleteUser(int userId);
  
}