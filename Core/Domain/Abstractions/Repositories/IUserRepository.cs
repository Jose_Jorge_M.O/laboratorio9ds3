using Domain.Entities;

namespace Domain.Abtractions.Repositories;

public interface IUserRepository
{
  public User GetUserByUsernameAndPassword(string username, string password);
  public IEnumerable<User> GetUsers();
  public User Create(User user);
  public void Delete(int userId);
}