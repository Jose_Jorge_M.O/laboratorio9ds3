using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Entities;

namespace Domain.Abtractions.Repositories
{
  public interface IGameRepository
  {
    Game Create(Game game);

    Game GetById(int id);

    IEnumerable<Game> GetAll();

    void Update(Game game);

    void Delete(int id);
  }
}