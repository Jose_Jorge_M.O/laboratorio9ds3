namespace Domain.Exceptions;

public class ApiException : Exception
{
  public string Title { get; }
  public int Status { get; }

  public ApiException(string title, string message, int status) : base(message)
  {
    Title = title;
    Status = status;
  }
}