namespace Domain.Exceptions;

public class ApiNotFoundException : ApiException
{
  public ApiNotFoundException(string title, string message, int status) : base(title, message, status) { }
 
  public ApiNotFoundException(string title, string message) : base(title, message, 404) { }
}