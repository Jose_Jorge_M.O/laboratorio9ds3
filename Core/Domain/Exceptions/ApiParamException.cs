namespace Domain.Exceptions;

public class ApiParamException : ApiException
{
 public ApiParamException(string title, string message, int status) : base(title, message, status) { }
 
 public ApiParamException(string title, string message) : base(title, message, 400) { }
}