namespace Domain.Exceptions;

public class ApiDataConflictException : ApiException
{
  public ApiDataConflictException(string title, string message, int status) : base(title, message, status) { }
 
  public ApiDataConflictException(string title, string message) : base(title, message, 409) { }
}