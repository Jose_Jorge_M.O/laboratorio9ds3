using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Domain.Entities;

public class Game
{
  public int Id { get; set; }
  public string Name { get; set; } = string.Empty;
  [JsonConverter(typeof(JsonStringEnumConverter))]
  public Platform Platform { get; set; }
  [Required]
  [Range(0, double.MaxValue)]
  public decimal Price { get; set; }
}

public enum Platform
{
  Xbox,
  PlayStation
}