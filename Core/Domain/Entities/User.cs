using System.Text.Json.Serialization;

namespace Domain.Entities;

public class User
{
  public int Id { get; set; }
  public string Username { get; set; }
  public string Password { get; set; }
  [JsonConverter(typeof(JsonStringEnumConverter))]
  public Roles Role { get; set; }
}

public enum Roles
{
  Admin,
  User,
}